import React from "react";
import logo from "../../../assets/img/logo.png"
const Logo = () => {
    return (
        <img className='w-[229px]' src={logo} alt="" />
    )
}
export default Logo