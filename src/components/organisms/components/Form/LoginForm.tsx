
import { Alert, Button, Form, Input } from "antd";
import { useForm } from "antd/lib/form/Form";
// import { signIn } from "next-auth/react";

import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { login } from "../../../../apis/apis";
import { PHONE, TOKEN } from "../../../../configs/localStorage";
import { Notification } from "../../../../configs/notification";


const LoginForm = () => {
    const Navigate = useNavigate()
    const [form] = useForm();
    const [isLoading, setIsLoading] = useState(false);
    const handleFinish = () => {

    }
    return (
        <>
            <Form form={form} layout="vertical" onFinish={handleFinish}>

                <Form.Item label="Enter Phone Number" name="phonenb" rules={[{ message: "Please enter your phone number" }]}>
                    <Input size="large" placeholder="Phone Number" />
                </Form.Item>

                <Form.Item label="Enter Your Password" name="pass" rules={[{ message: "Please enter a password" }]}>
                    <Input.Password placeholder="Password" type="password" size="large" />
                </Form.Item>
                <Link to="/forgot-password" className="flex  justify-end	mt-2 text-[#4285F4]">Forgot Password</Link>
                <Button
                    htmlType="submit"
                    className="!h-12	my-0	mx-auto	!py-4	!flex justify-center items-center w-2/5 !border-inherit !bg-[#349E49F0] !text-base	!rounded-xl !text-white font-medium	"
                    loading={isLoading}
                >
                    Login
                </Button>
            </Form>
        </>
    );
};

export default React.memo(LoginForm);
